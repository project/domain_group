<?php

namespace Drupal\domain_group\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\domain_group\Plugin\DomainGroupSettingsManager;
use Drupal\group\Entity\GroupInterface;

/**
 * Class DomainGroupSettingsForm.
 */
class DomainGroupSettingsForm extends FormBase {

  /**
   * The DomainGroupSettingsManager service.
   *
   * @var \Drupal\domain_group\Plugin\DomainGroupSettingsManager
   */
  protected $pluginManagerDomainGroupSettings;

  /**
   * Constructs a new DomainGroupSettingsForm object.
   */
  public function __construct(DomainGroupSettingsManager $plugin_manager_domain_group_settings) {
    $this->pluginManagerDomainGroupSettings = $plugin_manager_domain_group_settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.domain_group_settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_group_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, GroupInterface $group = NULL) {
    $form['tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('@group_label - Domain Settings', ['@group_label' => $group->label()]),
    ];
    foreach ($this->pluginManagerDomainGroupSettings->getAll() as $plugin_id => $plugin) {
      $form[$plugin_id] = [
        '#type' => 'details',
        '#title' => $plugin->getLabel(),
        '#group' => 'tabs',
      ] + $plugin->buildConfigurationForm([], $form_state, $group);
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->pluginManagerDomainGroupSettings->getAll() as $plugin) {
      $plugin->validateConfigurationForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($this->pluginManagerDomainGroupSettings->getAll() as $plugin) {
      $plugin->submitConfigurationForm($form, $form_state);
    }
    $this->messenger()->addStatus('Changes saved');
  }

}
