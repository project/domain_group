<?php

namespace Drupal\domain_group\Plugin\DomainGroupSettings;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\domain\DomainStorageInterface;
use Drupal\domain_group\Plugin\DomainGroupSettingsBase;
use Drupal\group\Entity\GroupInterface;
use Drupal\domain\DomainValidatorInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\domain\Entity\Domain;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Provides options for group domain.
 *
 * @DomainGroupSettings(
 *   id = "domain_group_site_settings",
 *   label = @Translation("Site Settings"),
 * )
 */
class GroupDomain extends DomainGroupSettingsBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The path alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The domain validator.
   *
   * @var \Drupal\domain\DomainValidatorInterface
   */
  protected $validator;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The domain entity storage.
   *
   * @var \Drupal\domain\DomainStorageInterface
   */
  protected $domainStorage;

  /**
   * The domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DomainValidatorInterface $validator, RendererInterface $renderer, ConfigFactoryInterface $config_factory, AliasManagerInterface $alias_manager, PathValidatorInterface $path_validator, DomainStorageInterface $domain_storage, DomainNegotiatorInterface $domain_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->validator = $validator;
    $this->renderer = $renderer;
    $this->configFactory = $config_factory;
    $this->aliasManager = $alias_manager;
    $this->pathValidator = $path_validator;
    $this->domainStorage = $domain_storage;
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('domain.validator'),
      $container->get('renderer'),
      $container->get('config.factory'),
      $container->get('path.alias_manager'),
      $container->get('path.validator'),
      $container->get('entity_type.manager')->getStorage('domain'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, GroupInterface $group) {
    $site_config = $this->configFactory->get('system.site');
    $config = $this->configFactory->get('domain_site_settings.domainconfigsettings');
    $domain = Domain::load('group_' . $group->id());
    $site_mail = $site_config->get('mail');
    if (empty($site_mail)) {
      $site_mail = ini_get('sendmail_from');
    }
    if ($domain && $config->get($domain->id()) != NULL) {
      $site_mail = $config->get($domain->id() . '.site_mail');
    }
    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#size' => 40,
      '#maxlength' => 80,
      '#description' => $this->t('The canonical hostname, using the full <em>subdomain.example.com</em> format. Leave off the http:// and the trailing slash and do not include any paths.<br />If this domain uses a custom http(s) port, you should specify it here, e.g.: <em>subdomain.example.com:1234</em><br />The hostname may contain only lowercase alphanumeric characters, dots, dashes, and a colon (if using alternative ports).'),
      '#default_value' => isset($domain) ? $domain->getHostname() : '',
    ];
    if ($domain) {
      $form['empty_host_message'] = [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="hostname"]' => ['empty' => TRUE],
          ],
        ],
        'empty_host' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('Leaving empty the hostname will invalidate all the configutation in this form.'),
          '#attributes' => [
            'class' => ['color-error'],
          ],
        ],
      ];
    }
    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Domain status'),
      '#options' => [1 => $this->t('Active'), 0 => $this->t('Inactive')],
      '#description' => $this->t('"Inactive" domains are only accessible to user roles with that assigned permission.'),
      '#default_value' => isset($domain) ? (int) $domain->get('status') : TRUE,
    ];
    $form['site_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site name'),
      '#default_value' => (isset($domain) && $config->get($domain->id()) !== NULL) ? $config->get($domain->id() . '.site_name') : $group->label(),
      '#required' => TRUE,
    ];
    $form['site_slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#default_value' => (isset($domain) && $config->get($domain->id()) !== NULL) ? $config->get($domain->id() . '.site_slogan') : $site_config->get('slogan'),
      '#description' => $this->t("How this is used depends on your site's theme."),
    ];
    $form['site_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email address'),
      '#default_value' => $site_mail,
      '#description' => $this->t("The <em>From</em> address in automated emails sent during registration and new password requests, and other notifications. (Use an address ending in your site's domain to help prevent this email being flagged as spam.)"),
      '#required' => TRUE,
    ];
    $form['error_page'] = [
      '#type' => 'details',
      '#title' => $this->t('Error pages'),
      '#open' => TRUE,
    ];
    $form['error_page']['site_403'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default 403 (access denied) page'),
      '#default_value' => (isset($domain) && $config->get($domain->id()) !== NULL) ? $config->get($domain->id() . '.site_403') : $site_config->get('page.403'),
      '#size' => 40,
      '#description' => $this->t('This page is displayed when the requested document is denied to the current user. Leave blank to display a generic "access denied" page.'),
    ];
    $form['error_page']['site_404'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default 404 (not found) page'),
      '#default_value' => (isset($domain) && $config->get($domain->id()) !== NULL) ? $config->get($domain->id() . '.site_404') : $site_config->get('page.404'),
      '#size' => 40,
      '#description' => $this->t('This page is displayed when no other content matches the requested document. Leave blank to display a generic "page not found" page.'),
    ];
    $form_state->set('group', $group);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Avoid group domains if default domain does not exist.
    if (!$this->domainStorage->loadDefaultDomain()) {
      $form_state->setErrorByName('hostname', $this->t('In order to enable this Organization domain, a Default one should be set. Please go to the <a href="@url">Domain records</a> page and create a Default Domain for the main site <i>(@host_name)</i>.', [
        '@url' => '/admin/config/domain',
        '@host_name' => $this->domainNegotiator->getHttpHost(),
      ]));
    }

    $hostname = $form_state->getValue('hostname');
    if ($hostname) {
      $errors = $this->validator->validate($hostname);
      $existing = $this->domainStorage->loadByProperties(['hostname' => $hostname]);
      $existing = reset($existing);
      // If we have already registered a hostname,
      // make sure we don't create a duplicate.
      $group = $form_state->get('group');
      if ($existing && $existing->id() != 'group_' . $group->id() && $existing->getDomainId() != $hostname) {
        $form_state->setErrorByName('hostname', $this->t('The hostname is already registered.'));
      }
      if (!empty($errors)) {
        // Render errors to display as message.
        $message = [
          '#theme' => 'item_list',
          '#items' => $errors,
        ];
        $message = $this->renderer->renderPlain($message);
        $form_state->setErrorByName('hostname', $message);
      }
    }

    // Get the normal paths of both error pages.
    if (!$form_state->isValueEmpty('site_403')) {
      $form_state->setValueForElement($form['domain_group_site_settings']['error_page']['site_403'], $this->aliasManager->getPathByAlias($form_state->getValue('site_403')));
    }
    if (!$form_state->isValueEmpty('site_404')) {
      $form_state->setValueForElement($form['domain_group_site_settings']['error_page']['site_404'], $this->aliasManager->getPathByAlias($form_state->getValue('site_404')));
    }
    if (($value = $form_state->getValue('site_403')) && $value[0] !== '/') {
      $form_state->setErrorByName('site_403', $this->t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_403')]));
    }
    if (($value = $form_state->getValue('site_404')) && $value[0] !== '/') {
      $form_state->setErrorByName('site_404', $this->t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_404')]));
    }
    // Validate 403 error path.
    if (!$form_state->isValueEmpty('site_403') && !$this->pathValidator->isValid($form_state->getValue('site_403'))) {
      $form_state->setErrorByName('site_403', $this->t("The path '%path' is either invalid or you do not have access to it.", ['%path' => $form_state->getValue('site_403')]));
    }
    // Validate 403 for restrict_login.
    $restricted_login = $this->configFactory->getEditable('domain_group.settings')->get('restricted_login');
    // If restricted login is set, avoid 403 page to redirect to /user/login.
    if (!$form_state->isValueEmpty('site_403') && $form_state->getValue('site_403') == '/user/login' && $restricted_login) {
      // General settings config form with query to get the Admins back here.
      $settings_page = Url::fromRoute('domain_group.domain_group_general_form', [], [
        'query' => [
          'destination' => Url::fromRoute('<current>')->toString(),
        ],
      ])->toString();
      $form_state->setErrorByName('site_403', $this->t("Redirecting 403 to login page is not supported by the Restricted Login setting.<br />
        Consider leaving this field blank or disable the Restricted Login in the <a href=':general_settings_page'>General Settings form</a>.", [
          ':general_settings_page' => $settings_page,
        ]));
    }
    // Validate 404 error path.
    if (!$form_state->isValueEmpty('site_404') && !$this->pathValidator->isValid($form_state->getValue('site_404'))) {
      $form_state->setErrorByName('site_404', $this->t("The path '%path' is either invalid or you do not have access to it.", ['%path' => $form_state->getValue('site_404')]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $group = $form_state->get('group');
    if ($form_state->getValue('hostname')) {
      if ($domain = Domain::load('group_' . $group->id())) {
        $domain->setHostname($form_state->getValue('hostname'));
        $domain->set('status', $form_state->getValue('status'));
      }
      else {
        $domain = Domain::create([
          'id' => 'group_' . $group->id(),
          'name' => $group->label(),
          'hostname' => $form_state->getValue('hostname'),
          'scheme' => 'variable',
          'status' => $form_state->getValue('status'),
          'is_default' => FALSE,
        ]);
      }
      $domain->save();
      // Site settings.
      $domain_id = $domain->id();
      $site_name = !empty($form_state->getValue('site_name')) ? $form_state->getValue('site_name') : $group->label();
      $site_slogan = $form_state->getValue('site_slogan');
      $site_mail = $form_state->getValue('site_mail');
      $site_frontpage = '/group/' . $group->id();
      $site_403 = $form_state->getValue('site_403');
      $site_404 = $form_state->getValue('site_404');
      $config = $this->configFactory->getEditable('domain_site_settings.domainconfigsettings');
      $config->set($domain_id . '.site_name', $site_name);
      $config->set($domain_id . '.site_slogan', $site_slogan);
      $config->set($domain_id . '.site_mail', $site_mail);
      $config->set($domain_id . '.site_frontpage', $site_frontpage);
      $config->set($domain_id . '.site_403', $site_403);
      $config->set($domain_id . '.site_404', $site_404);
      $config->save();
    }
    else {
      if ($domain = Domain::load('group_' . $group->id())) {
        $domain->delete();
      }
    }
  }

}
