<?php

namespace Drupal\domain_group\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Domain group settings plugins.
 */
interface DomainGroupSettingsInterface extends PluginInspectionInterface {

  /**
   * Returns the administrative label for the plugin.
   *
   * @return string
   *   The plugin label.
   */
  public function getLabel();

  /**
   * Returns the plugin provider.
   *
   * @return string
   *   The plugin provider.
   */
  public function getProvider();

}
